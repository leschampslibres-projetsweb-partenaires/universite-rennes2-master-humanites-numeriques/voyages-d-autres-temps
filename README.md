# VOYAGES D'AUTRES TEMPS

Voyages d'autres temps retrace le parcours d'un enfant à la suite du décès de sa grand-mère, en cherchant à se souvenir d'elle à l'aide de sa mère et de différents objets, images et films.<br /><br />
Un projet en partenariat avec [Les Champs Libres](https://leschampslibres.fr), [le Musée de Bretagne](https://www.musee-bretagne.fr), [la Cinémathèque de Bretagne](https://www.cinematheque-bretagne.bzh) et [l'Université Rennes 2](https://sites-formations.univ-rennes2.fr/master-humanitesnumeriques/presentation/)

---

### Live Démo
:computer: https://projetsweb.leschampslibres.fr/...

### Éditorial
- À suivre sur https://twitter.com/AutresVoyage
- Blog du Master : https://masterhnr2.hypotheses.org

### Contributeur.trice.s
Anaïs Hénin, Marion Ignace, Jonathan Georges, Hugo Troadec, Léonard Begasse

### Technologies & logiciels
- HTML5, JSON, JS
- [RPG Maker](https://www.rpgmakerweb.com), [Adobe Photoshop](https://www.adobe.com/fr/products/photoshop.html), [Adobe Premiere Pro](https://www.adobe.com/fr/products/premiere.html)

[![Licence GNU/GPLv3](https://gitlab.com/leschampslibres-projetsweb-partenaires/universite-rennes2-master-humanites-numeriques/voyages-d-autres-temps/-/raw/master/gplv3-127x51.png)](https://www.gnu.org/licenses/quick-guide-gplv3.html)

---

> **CONTACT**<br />
> [Université Rennes 2<br />
> UFR Sciences sociales<br />
> Master Humanités Numériques](https://sites-formations.univ-rennes2.fr/master-humanitesnumeriques/contacts/)<br />
> :mortar_board: Promotion Hedy Lamarr | Master 2 2020-2021

